package com.redissdk.redis;

import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.redissdk.redis.common.ms.ShardedMasterSlaveJedis;
import com.redissdk.redis.common.ms.ShardedMasterSlaveJedisSentinelPool;

import redis.clients.jedis.JedisPoolConfig;
import redis.clients.util.Pool;

/**
 * 项目名称:[redisx] 
 * 包:[com.redissdk.redis] 
 * 文件名称:[RedisSharedMasterSlaveUtil]
 * 描述:[Redis 工具类 - RedisSharedMasterSlave] 
 * 创建人:[陆文斌] 
 * 创建时间:[2016年12月5日 下午4:43:00]
 * 修改人:[陆文斌] 
 * 修改时间:[2016年12月5日 下午4:43:00] 
 * 修改备注:[说明本次修改内容] 
 * 版权所有:luwenbin006@163.com
 * 版本:[v1.0]
 */
public class RedisSharedMasterSlaveSentinelUtil
{

    private static Logger logger = Logger.getLogger(RedisSharedMasterSlaveSentinelUtil.class);

    // redis_sentinel服务器IP
    private static String ADDR_ARRAY = null; // 192.168.0.1:63781,192.168.0.1:63782

    private static String MASTER_NAME = null; // 192.168.0.1:63781,192.168.0.1:63782

    // redis_sentinel的端口号
    // private static int PORT =
    // FileUtil.getPropertyValueInt("properties/redis_sentinel.properties",
    // "port");

    // 访问密码
    private static String AUTH = null;

    // 可用连接实例的最大数目，默认值为8；
    // 如果赋值为-1，则表示不限制；如果pool已经分配了maxActive个jedis实例，则此时pool的状态为exhausted(耗尽)。
    private static int MAX_ACTIVE = 0;

    // 控制一个pool最多有多少个状态为idle(空闲的)的jedis实例，默认值也是8。
    private static int MAX_IDLE = 0;

    // 等待可用连接的最大时间，单位毫秒，默认值为-1，表示永不超时。如果超过等待时间，则直接抛出JedisConnectionException；
    private static int MAX_WAIT = 0;

    // 超时时间
    private static int TIMEOUT = 0;

    // 在borrow一个jedis实例时，是否提前进行validate操作；如果为true，则得到的jedis实例均是可用的；
    private static boolean TEST_ON_BORROW = false;

    private static Pool<ShardedMasterSlaveJedis> shardedMasterSlaveJedisPool = null;

    /**
     * redis过期时间,以秒为单位
     */
    private final static int EXRP_HOUR = 60 * 60; // 一小时
    private final static int EXRP_DAY = 60 * 60 * 24; // 一天
    private final static int EXRP_MONTH = 60 * 60 * 24 * 30; // 一个月

    private static void initRedisConfigValue()
    {
        ADDR_ARRAY = FileUtil.getPropertyValue("properties/redis_sentinel.properties", "server"); // 192.168.0.1:63781,192.168.0.1:63782

        MASTER_NAME = FileUtil.getPropertyValue("properties/redis_sentinel.properties", "master_name"); // 192.168.0.1:63781,192.168.0.1:63782

        AUTH = FileUtil.getPropertyValue("properties/redis_sentinel.properties", "auth");

        MAX_ACTIVE = FileUtil.getPropertyValueInt("properties/redis_sentinel.properties", "max_active");

        MAX_IDLE = FileUtil.getPropertyValueInt("properties/redis_sentinel.properties", "max_idle");

        MAX_WAIT = FileUtil.getPropertyValueInt("properties/redis_sentinel.properties", "max_wait");

        TIMEOUT = FileUtil.getPropertyValueInt("properties/redis_sentinel.properties", "timeout");

        TEST_ON_BORROW = FileUtil.getPropertyValueBoolean("properties/redis_sentinel.properties", "test_on_borrow");

        FileUtil.remove("properties/redis_sentinel.properties");
    }

    /**
     * 初始化Redis连接池
     */
    private static void initialPool()
    {

        initRedisConfigValue();

        JedisPoolConfig config = new JedisPoolConfig();
        config.setMaxTotal(MAX_ACTIVE);
        config.setMaxIdle(MAX_IDLE);
        config.setMaxWaitMillis(MAX_WAIT);
        config.setTestOnBorrow(TEST_ON_BORROW);

        Set<String> sentinels = new LinkedHashSet<String>();
        if (!StringUtils.isEmpty(ADDR_ARRAY))
        {
            String[] address = ADDR_ARRAY.split(",");
            for (String add : address)
            {
                sentinels.add(add);
            }
        }
        // sentinels.add("127.0.0.1:63792");

        Set<String> masterNames = new LinkedHashSet<String>();
        masterNames.add(MASTER_NAME);

        shardedMasterSlaveJedisPool = new ShardedMasterSlaveJedisSentinelPool(masterNames, sentinels, config,TIMEOUT,AUTH);

        // ShardedMasterSlaveJedis shardedMasterSlaveJedis =
        // shardedshardedMasterSlaveJedisPool.getResource();

        // MasterSlaveJedis masterSlaveJedis =
        // shardedMasterSlaveJedisPool.getResource();
        // masterSlaveJedis.auth(AUTH);
        // //>>> masterSlaveJedis = MasterSlaveJedis
        // {master=192.168.137.101:6379, slaves=[192.168.137.101:6380,
        // 192.168.137.101:6381]}
        // System.out.println(">>> masterSlaveJedis = " + masterSlaveJedis);
        //
        // masterSlaveJedis.set("nowTime", "2015-03-16 15:34:55"); // The
        // underlying actually call the master.set("nowTime", "2015-03-16
        // 15:34:55");
        //
        // LockSupport.parkNanos(TimeUnit.MILLISECONDS.toNanos(200));
        //
        // String slaveHolder1 = "myslave1";
        // Jedis slave1 = masterSlaveJedis.opsForSlave(slaveHolder1); // if no
        // any slave found, opsForSlave() will return master as a slave to be
        // use
        // System.out.println(">>> nowTime = " + slave1.get("nowTime")); //>>>
        // nowTime = 2015-03-16 15:34:55
        //
        // String slaveHolder2 = "myslave1";
        // Jedis slave2 = masterSlaveJedis.opsForSlave(slaveHolder2);
        // System.out.println(">>> nowTime = " + slave2.get("nowTime")); //>>>
        // nowTime = 2015-03-16 15:34:55
        //
        // System.out.println(slave1.equals(slave2)); // must be true if
        // slaveHolder1 equals slaveHolder2
        //
        // shardedMasterSlaveJedisPool.returnResource(masterSlaveJedis);

    }

    /**
     * 在多线程环境同步初始化
     */
    private static synchronized void poolInit()
    {
        if (shardedMasterSlaveJedisPool == null)
        {
            initialPool();
        }
    }

    /**
     * 同步获取Jedis实例
     * 
     * @return Jedis
     */
    private synchronized static ShardedMasterSlaveJedis getJedis()
    {
        if (shardedMasterSlaveJedisPool == null)
        {
            poolInit();
        }
        ShardedMasterSlaveJedis jedis = null;
        try
        {
            if (shardedMasterSlaveJedisPool != null)
            {
                jedis = shardedMasterSlaveJedisPool.getResource();
            }
        }
        catch (Exception e)
        {
            returnResource(jedis);
            logger.error("Get jedis error : " + e);
        }
        finally
        {

        }

        return jedis;
    }

    /**
     * 释放jedis资源
     * 
     * @param jedis
     */
    private static void returnResource(final ShardedMasterSlaveJedis jedis)
    {
        if (jedis != null)
        {
            jedis.close();
        }
    }

    /**
     * 释放jedis资源
     * 
     * @param jedis
     */
    @Deprecated
    private static void returnBrokenResource(final ShardedMasterSlaveJedis jedis)
    {
        if (jedis != null && null != shardedMasterSlaveJedisPool)
        {
            shardedMasterSlaveJedisPool.returnBrokenResource(jedis);
        }
    }

    /**
     * 设置 String
     * 
     * @param key
     * @param value
     */
    public static String setString(String key, String value)
    {
        ShardedMasterSlaveJedis shardedMasterSlaveJedis = null;
        String r_str = "nil";
        try
        {
            value = StringUtils.isEmpty(value) ? "" : value;
            shardedMasterSlaveJedis = getJedis();
            r_str = shardedMasterSlaveJedis.set(key, value);
        }
        catch (Exception e)
        {
            logger.error("Set key error : " + e);
        }
        finally
        {
            returnResource(shardedMasterSlaveJedis);
        }

        return r_str;
    }

    /**
     * 设置 过期时间
     * 
     * @param key
     * @param seconds
     *            以秒为单位
     * @param value
     */
    public static String setString(String key, String value, int expirationTime)
    {
        ShardedMasterSlaveJedis shardedMasterSlaveJedis = null;
        String r_str = "nil";
        try
        {
            value = StringUtils.isEmpty(value) ? "" : value;
            shardedMasterSlaveJedis = getJedis();
            r_str = shardedMasterSlaveJedis.setex(key, expirationTime, value);
        }
        catch (Exception e)
        {
            logger.error("Set keyex error : " + e);
        }
        finally
        {
            returnResource(shardedMasterSlaveJedis);
        }

        return r_str;
    }

    public static Set<byte[]> getKeys(byte[] keys)
    {
        ShardedMasterSlaveJedis shardedMasterSlaveJedis = getJedis();
        Set<byte[]> set_ = shardedMasterSlaveJedis.getShard(keys).keys(keys);
        returnResource(shardedMasterSlaveJedis);
        return set_;
    }

    public static Set<String> getKeys(String keys)
    {
        ShardedMasterSlaveJedis shardedMasterSlaveJedis = getJedis();
        Set<String> set_ = shardedMasterSlaveJedis.getShard(keys).keys(keys);
        returnResource(shardedMasterSlaveJedis);
        return set_;
    }

    public static Set<byte[]> hkeys(byte[] hkeys)
    {
        ShardedMasterSlaveJedis shardedMasterSlaveJedis = getJedis();
        Set<byte[]> set_ = shardedMasterSlaveJedis.getShard(hkeys).hkeys(hkeys);
        returnResource(shardedMasterSlaveJedis);
        return set_;
    }

    public static Set<String> hkeys(String hkeys)
    {
        ShardedMasterSlaveJedis shardedMasterSlaveJedis = getJedis();
        Set<String> set_ = shardedMasterSlaveJedis.getShard(hkeys).hkeys(hkeys);
        returnResource(shardedMasterSlaveJedis);
        return set_;

    }

    public static Collection<byte[]> hvals(byte[] hvals)
    {
        ShardedMasterSlaveJedis shardedMasterSlaveJedis = getJedis();
        Collection<byte[]> collection = shardedMasterSlaveJedis.getShard(hvals).hvals(hvals);
        returnResource(shardedMasterSlaveJedis);
        return collection;
    }

    public static List<String> hvals(String hvals)
    {
        ShardedMasterSlaveJedis shardedMasterSlaveJedis = getJedis();
        List<String> list = shardedMasterSlaveJedis.getShard(hvals).hvals(hvals);
        returnResource(shardedMasterSlaveJedis);
        return list;
    }

    /**
     * 获取String值
     * 
     * @param key
     * @return value
     */
    public static String getString(String key)
    {
        ShardedMasterSlaveJedis shardedMasterSlaveJedis = getJedis();
        String r_str = null;
        if (shardedMasterSlaveJedis == null || !shardedMasterSlaveJedis.exists(key))
        {
            r_str = null;
            if (null != shardedMasterSlaveJedis)
            {
                returnResource(shardedMasterSlaveJedis);
            }
        }
        else
        {
            r_str = shardedMasterSlaveJedis.get(key);
        }
        returnResource(shardedMasterSlaveJedis);
        return r_str;
    }

    public static String setObject(String key, Object obj, int expirationTime)
    {
        String r_str = null;

        if (null == obj)
        {
            r_str = "nil";
        }
        else
        {
            ShardedMasterSlaveJedis shardedMasterSlaveJedis = null;
            try
            {
                shardedMasterSlaveJedis = getJedis();
                r_str = shardedMasterSlaveJedis.setex(key.getBytes(), expirationTime,
                        SerializationAndCompressUtils.fastSerialize(obj));

            }
            catch (Exception e)
            {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            finally
            {

                returnResource(shardedMasterSlaveJedis);

            }
        }

        return r_str;

    }

    public static String setObject(String key, Object obj)
    {
        String r_str = null;

        if (null == obj)
        {
            r_str = "nil";
        }
        else
        {
            ShardedMasterSlaveJedis shardedMasterSlaveJedis = null;
            try
            {

                shardedMasterSlaveJedis = getJedis();
                r_str = shardedMasterSlaveJedis.set(key.getBytes(), SerializationAndCompressUtils.fastSerialize(obj));

            }
            catch (Exception e)
            {
                // return jedis object to pll and set jedis is null.
                returnResource(shardedMasterSlaveJedis);
                shardedMasterSlaveJedis = null;
                e.printStackTrace();
            }
            finally
            {

                returnResource(shardedMasterSlaveJedis);

            }
        }

        return r_str;

    }

    public static Long del(String key)
    {
        Long r_l = 0l;
        ShardedMasterSlaveJedis shardedMasterSlaveJedis = null;
        try
        {
            shardedMasterSlaveJedis = getJedis();
            r_l = shardedMasterSlaveJedis.del(key);

        }
        catch (Exception e)
        {
            // return jedis object to pll and set jedis is null.
            returnResource(shardedMasterSlaveJedis);
            shardedMasterSlaveJedis = null;
            e.printStackTrace();
        }
        finally
        {

            returnResource(shardedMasterSlaveJedis);

        }
        return r_l;

    }

    public static Long del(byte[] key)
    {
        Long r_l = 0l;
        ShardedMasterSlaveJedis shardedMasterSlaveJedis = null;
        try
        {
            shardedMasterSlaveJedis = getJedis();
            r_l = shardedMasterSlaveJedis.del(key);

        }
        catch (Exception e)
        {
            // return jedis object to pll and set jedis is null.
            returnResource(shardedMasterSlaveJedis);
            shardedMasterSlaveJedis = null;
            e.printStackTrace();
        }
        finally
        {

            returnResource(shardedMasterSlaveJedis);

        }
        return r_l;

    }

    /**
     * 获取Object
     * 
     * @param key
     * @return
     * @return value
     */
    public static <T> T getObject(String key, Class<T> classz)
    {
        ShardedMasterSlaveJedis shardedMasterSlaveJedis = getJedis();

        T t = null;

        if (shardedMasterSlaveJedis == null || !shardedMasterSlaveJedis.exists(key.getBytes()))
        {
            t = null;
            if (null != shardedMasterSlaveJedis)
            {
                returnResource(shardedMasterSlaveJedis);
            }
        }
        else
        {
            byte[] value = shardedMasterSlaveJedis.get(key.getBytes());
            try
            {

                if (null == value)
                {
                    t = null;
                }
                else
                {
                    t = classz.cast(SerializationAndCompressUtils.fastDeserialize(value));
                }
            }
            catch (Exception e)
            {
                // return jedis object to pll and set jedis is null.
                returnResource(shardedMasterSlaveJedis);
                shardedMasterSlaveJedis = null;
                e.printStackTrace();
            }
            finally
            {

                returnResource(shardedMasterSlaveJedis);

            }
        }

        return t;
    }

    public static Long hdel(String key, String field)
    {
        ShardedMasterSlaveJedis shardedMasterSlaveJedis = getJedis();
        Long r_l = 0l;
        if (shardedMasterSlaveJedis == null)
        {
            r_l = 0l;
        }
        else
        {
            try
            {
                r_l = shardedMasterSlaveJedis.hdel(key, field);
            }
            catch (Exception e)
            {
                // return jedis object to pll and set jedis is null.
                returnResource(shardedMasterSlaveJedis);
                shardedMasterSlaveJedis = null;
                e.printStackTrace();
            }
            finally
            {

                returnResource(shardedMasterSlaveJedis);

            }
        }
        return r_l;
    }

    public static Map<String, String> hgetall(String key)
    {
        ShardedMasterSlaveJedis shardedMasterSlaveJedis = getJedis();
        Map<String, String> map = null;
        if (shardedMasterSlaveJedis == null)
        {
            map = null;
        }
        else
        {
            try
            {

                map = shardedMasterSlaveJedis.hgetAll(key);

            }
            catch (Exception e)
            {
                // return jedis object to pll and set jedis is null.
                returnResource(shardedMasterSlaveJedis);
                shardedMasterSlaveJedis = null;
                e.printStackTrace();
            }
            finally
            {

                returnResource(shardedMasterSlaveJedis);

            }
        }

        return map;
    }

    public static Long hincrBy(String key, String field, long value)
    {
        ShardedMasterSlaveJedis shardedMasterSlaveJedis = getJedis();
        Long r_l = 0l;
        if (shardedMasterSlaveJedis == null)
        {
            r_l = 0l;
        }
        else
        {
            try
            {
                r_l = shardedMasterSlaveJedis.hincrBy(key, field, value);
            }
            catch (Exception e)
            {
                // return jedis object to pll and set jedis is null.
                returnResource(shardedMasterSlaveJedis);
                shardedMasterSlaveJedis = null;
                e.printStackTrace();
            }
            finally
            {

                returnResource(shardedMasterSlaveJedis);

            }
        }

        return r_l;
    }

}
